<?php 
/**
* 	Josemar da Costa Magalhães
*		Sábado, 24 de Agosto de 2013	
*/


define('INIT_COMMAND', 'git init');
define('LOG_COMMAND', 'git log');
class GitRepo{

	public $path;
	public $name;
	public $commits = [];
	function __construct($path, $name){
		$this->path = $path;
		$this->name = $name;
		$this->repo = "{$this->path}{$this->name}";
	}

	public function init(){
		mkdir($this->repo);
		$output = [];
		$return_values = [];
		$this->execute($this->repo, INIT_COMMAND, $output, $return_values);
		return ['output' => $output, 'return_values' => $return_values];
	}

	public function log(){
		$output = [];
		$return_values = [];
		$this->execute($this->repo, LOG_COMMAND, $output, $return_values);
		$this->parseOutput($output);
		return ['output' => $output, 'return_values' => $return_values];	
	}

	private function parseOutput($output){
		$count = count($output);
		for($i = 0; $i < $count; $i = $i + 6){
			$this->commits[] = new Commit($output[$i], $output[$i+1], $output[$i+2], $output[$i+4]);
		}
	}

	private function execute($where, $command, &$output, &$returnValue){
	  $cwd = getcwd();
	  chdir($where);
	  exec($command, $output, $returnValue);
	  chdir($cwd);
	}
}
class Commit{
	public $id;
	public $author;
	public $message;
	public $date;

	function __construct($id, $author, $date, $message){
		$this->set_id($id);
		$this->set_author($author);
		$this->set_date($date);
		// $this->date = $date;
		$this->set_message($message);
	}
	public function set_author($attr){
		$author_split = preg_split("/:/", $attr)[1];
		$name_split = preg_split("/</", $author_split);
		$name = trim($name_split[0]);
		$email = trim(preg_replace("/>/", "", $name_split[1]));
		$this->author = new Author($name, $email);
	}
	public function set_id($attr){
		$this->id = preg_split("/ /", $attr)[1];
	}
	public function set_message($attr){
		$this->message = trim($attr);
	}
	public function set_date($attr){
		$this->date = strtotime(trim(preg_split("/: /", $attr)[1]));
	}
}
class Author{
	public $name;
	public $email;
	function __construct($name, $email){
		$this->name = $name;
		$this->email = $email;
	}
}

 ?>