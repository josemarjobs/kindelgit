<?php 
$app->get('/repos', function() use ($app){
	$app->render('layout.php', [
		'view' => 'repositorios/index',
		'repos' => Repositorio::all()
	]);
});

$app->get('/repos/new', function() use ($app){
	$app->render('layout.php', [
		'view' => 'repositorios/new'
	]);
});
$app->post('/repos', function() use($app){
	$repositorio = new Repositorio();
	$repositorio->nome = $_POST['nome'];
	$repositorio->descricao = $_POST['descricao'];
	$repositorio->usuario_id = 1;	
	if ($repositorio->save()) {
		$git = new GitRepo(REPOS_PATH, $repositorio->nome);
		$result = $git->init();
		$app->flash('info', 'Repositorio criado com sucesso');
		// var_dump($result);
		$app->redirect("/repos/" . $repositorio->id);
	}else{
		$app->flash('error', 'Repositorio NAO FOI criado com sucesso');
		$app->redirect('/repos/new');
	}
});

$app->get('/repos/:id', function($id) use($app){
	try{
		$repo = Repositorio::find($id);

		$git = new GitRepo(REPOS_PATH, $repo->nome);
		$result = $git->log();

		$app->render('layout.php', [
			'view' => 'repositorios/show',
			'repo' => $repo,
			'commits' => $git->commits,
			'output' => $result['output'],
			'return_values' => $result['return_values']
		]);
	} catch(ActiveRecord\RecordNotFound $e){
		$app->flash('error', $e->getMessage());
		$app->redirect('/repos');
	}
});


?>