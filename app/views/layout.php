<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>KindelGit</title>
	<link rel="stylesheet" href="/app/assets/css/bootstrap3.min.css">
	<link rel="stylesheet" href="/app/assets/css/bootstrap.theme.css">
	<link rel="stylesheet" href="/app/assets/css/justified-nav.css">
	<link rel="stylesheet" href="/app/assets/css/app.css">
</head>
<body>
	<div id="container">

	<div class="masthead">
    <h1 class="text-muted"><a href="/">KindelGit</a></h1>
    <ul class="nav nav-justified">
      <li class="active"><a href="#">Home</a></li>
      <li><a href="/repos">Repositorios</a></li>
      <li><a href="#">Downloads</a></li>
      <li><a href="#">About</a></li>
      <li><a href="/login">Login</a></li>
      <li><a href="#">Contact</a></li>
    </ul>
  </div>

		<?php if (isset($flash['info'])): ?>
			<div class="alert alert-success">
				<p><?= $flash['info'] ?></p>
			</div>
		<?php endif ?>
		<?php if (isset($flash['error'])): ?>
			<div class="alert alert-danger">
				<p><?= $flash['error'] ?></p>
			</div>
		<?php endif ?>

		<?php if (isset($view)): ?>
			<?php include "{$view}.php" ?>
		<?php endif ?>	


    <!-- Site footer -->
    <div class="footer">
      <p>&copy; KindelBit Inovação 2013</p>
    </div>
	</div>
	
</body>
</html>