<section id="form-area">
	<form class="form-signin">
	  <h2 class="form-signin-heading">Please sign in</h2>
	  <input type="text" class="form-control" placeholder="Username or Email address" autofocus>
	  <input type="password" class="form-control" placeholder="Password">
	  <label class="checkbox">
	    <input type="checkbox" value="remember-me"> Remember me
	  </label>
	  <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
	</form>
</section>