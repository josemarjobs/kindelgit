<div class="jumbotron">
	<h1>Nossos Repositorios online</h1>
	<p class="lead">
		Muito simples e fácil de usar, apenas cadestre-se ou faça login, e prontos!
	</p>
	<p><a class="btn btn-lg btn-success" href="#">Cadastre-se</a></p>
</div>

<div class="row">
  <div class="col-lg-6">
    <h2>Crie Repositórios</h2>
    <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
    <p><a class="btn btn-primary" href="#">View details &raquo;</a></p>
  </div>
  <div class="col-lg-6">
    <h2>Membros e Colaboradores</h2>
    <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
    <p><a class="btn btn-primary" href="#">View details &raquo;</a></p>
 </div>
</div>