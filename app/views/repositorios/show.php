<div class="repositorio">
	<div class="panel panel-default">
		<div class="panel-heading">
			<h2 class="panel-title"><?= $repo->nome ?></h2>
		</div>
		<div class="panel-body">
			<p><?= $repo->descricao ?></p>
			
			<?php if (count($commits) == 0): ?>
				<h3>Empty Repo</h3>
			<?php else: ?>
			<table class="table">
				<thead>
					<th>#</th>
					<th>Autor</th>
					<th>Message</th>
					<th>Date</th>
				</thead>
				<?php foreach ($commits as $commit): ?>
				<tr>
					<td><?= substr($commit->id, 0, 6) ?></td>
					<td>
						<a href="/usuarios/"><?= $commit->author->name ?></a>
					</td>
					<td><?= $commit->message ?></td>
					<td><?= date("d/m/Y - H:m", $commit->date) ?></td>
				</tr>
				<?php endforeach ?>
			</table>
			<?php endif ?>

		</div>
	</div>
</div>