<h2 class="col-lg-offset-1 col-lg-11 criar-repo">Criar novo Repositorio</h2>
<form class="form-horizontal" method="post" action="/repos" role="form">
  <div class="form-group">
    <label for="nome" class="col-lg-2 control-label">Nome</label>
    <div class="col-lg-10">
      <input type="text" class="form-control" id="nome" name="nome" placeholder="Nome do Repositorio, sem espaços">
    </div>
  </div>
  <div class="form-group">
    <label for="inputPassword1" class="col-lg-2 control-label">Descrição</label>
    <div class="col-lg-10">
      <textarea name="descricao" id="descricao" class="form-control" cols="30" rows="5"></textarea>
    </div>
  </div>
  <div class="form-group">
    <div class="col-lg-offset-2 col-lg-10">
      <div class="checkbox">
        <label>
          <input type="checkbox"> Privado?
        </label>
      </div>
    </div>
  </div>
  <div class="form-group">
    <div class="col-lg-offset-2 col-lg-10">
      <button type="submit" class="btn btn-default">Criar Repositorio</button>
    </div>
  </div>
</form>