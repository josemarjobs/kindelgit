<h2>Repositorios</h2>
<table class="table">
	<thead>
		<th>#</th>
		<th>Usuario</th>
	</thead>
<?php foreach ($repos as $repo): ?>
	<tr>
		<td>
			<a href="/repos/<?= $repo->id ?>"><?= $repo->nome ?></a>
		</td>
		<td>
			<a href="/usuarios/<?= $repo->usuario_id ?>"><?= $repo->usuario_id ?></a>
		</td>
	</tr>
<?php endforeach ?>
</table>
<p>
	<a href="/repos/new" class="btn btn-primary">
		Criar Repositorio
	</a>
</p>
