drop database kindelgit;
create database kindelgit;
use kindelgit;

create table repositorios(
	id integer not null auto_increment primary key,
	nome varchar(100) not null,
	caminho varchar(255),
	descricao text,
	usuario_id integer,
	criado_em datetime
);

create table usuarios(
	id integer not null auto_increment primary key,
	username varchar(100) not null unique,
	email varchar(100) not null unique,
	password varchar(100) not null,
	nome varchar(255),
	criado_em datetime
);

