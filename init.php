<?php 
DEFINE('REPOS_PATH', realpath($_SERVER['DOCUMENT_ROOT'].'/../../') . "/kindelgit/repos/private/");

ActiveRecord\Config::initialize(function($cfg){
	$cfg->set_model_directory('./app/models');
	$cfg->set_connections(array('development' => 'mysql://root@localhost/kindelgit'));
});
?>