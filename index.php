<?php 
session_start();
require_once 'vendor/autoload.php';
require_once 'init.php';
require_once 'lib/git_repo.php';

function execute($where, $command, &$output, &$returnValue){
  $cwd = getcwd();
  chdir($where);
  exec($command, $output, $returnValue);
  chdir($cwd);
}

function cria_repositorio($nome, $local=REPOS_PATH){
	$repo = "{$local}{$nome}";
	mkdir($repo);
	$output = [];
	$return_values = [];
	execute($repo, "git init", $output, $return_values);
	return ['output' => $output, 'return_values' => $return_values];
}


$app = new Slim\Slim(['templates.path' => './app/views']);

$controller_path = './app/controllers';
$controllers_dir = dir($controller_path);
while(false !==($controller = $controllers_dir->read())){
  if ($controller==".." || $controller==".") { continue; }
  require_once "{$controller_path}/{$controller}";
}

$app->run();
 ?>